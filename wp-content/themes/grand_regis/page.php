<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package grand_regis
 */

get_header(); ?>

		<div id="primary" class="content-area">
			<main id="main" class="site-main" role="main">
				<div class="row">
					<div class="container">
						<div class="col s12 m6 offset-m6 l5 offset-l7">
							<form role="search" method="get" class="search-form" action="<?php echo home_url('/'); ?>">
								<label>
									<span class="screen-reader-text">Rechercher pour:</span>
									<input type="search" class="search-field" placeholder="Rechercher ..." value="" name="s"
										   title="Search for:"/>
								</label>
                                <button type="submit"><i class="fa fa-search fa-2x" aria-hidden="true"></i></button>
							</form>
						</div>
					</div>
				</div>


				<?php
				while (have_posts()) : the_post();
					//Cette section sera peu-être à modifier un coup que le plugin sera fait.  Juste à enlever les if/else
					if (is_front_page()) {
						get_template_part('template-parts/content', 'accueil');
					}
					elseif (is_page('Contact') || is_page('Programmation') ) {
						get_template_part( 'template-parts/content', $pagename );
					}
					else {
						get_template_part('template-parts/content', 'page');
					}

					// If comments are open or we have at least one comment, load up the comment template.
					//				if ( comments_open() || get_comments_number() ) :
					//					comments_template();
					//				endif;

				endwhile; // End of the loop.
				?>

			</main><!-- #main -->
		</div><!-- #primary -->

<?php
//get_sidebar();
get_footer();
