<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package grand_regis
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="container">
			<div class="row">
				<div class="col s12">
					<div class="wrapper">
						<div class="row">
							<div class="col center-on-small-only s12 m4 l3">
								<h4>adresse</h4>
								<span><strong>Le Grand Régis</strong></span><br>
								<span>3265 Ch Ste-Foy</span><br>
								<span>Ville de Québec</span><br>
								<span>QC G1X 1R9</span>
							</div>
							<div class="col center-on-small-only s12 m4 l3 offset-l2">
								<h4>billeterie</h4>
								<ul>
									<li>
										<a href="tel:418-780-1795">(418) 780-1795</a>
									</li>
									<li>
										<a href="mailto:info@legrandregis.com">info@legrandregis.com</a>
									</li>
								</ul>
							</div>
							<div class="col center-on-small-only s12 m4 l4">
								<div class="social right">
									<a href="http://www.facebook.com" target="_blank" class="social">
										<i class="fa fa-facebook" aria-hidden="true"></i>
									</a>
									<a href="http://www.instagram.com" target="_blank" class="social">
										<i class="fa fa-instagram" aria-hidden="true"></i>
									</a>
									<a href="http://www.twitter.com" target="_blank" class="social">
										<i class="fa fa-twitter" aria-hidden="true"></i>
									</a>
								</div>
							</div>
						</div>
						<div class="logo_simple">
							<object type="image/svg+xml"
									data="<?php echo get_template_directory_uri(); ?>/img/logo_simple.svg">
								<img src="<?php echo get_template_directory_uri(); ?>/img/logo_simple.png"
									 alt="logo simplifié du Grand Régis">
							</object>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
