<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package grand_regis
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="row">
				<div class="container">
					<div class="col s12 m6 offset-m6 l5 offset-l7">
						<form role="search" method="get" class="search-form" action="<?php echo home_url('/'); ?>">
							<label>
								<span class="screen-reader-text">Rechercher pour:</span>
								<input type="search" class="search-field" placeholder="Rechercher ..." value="" name="s"
									   title="Search for:"/>
							</label>
							<button type="submit"><i class="fa fa-search fa-2x" aria-hidden="true"></i></button>
						</form>
					</div>
				</div>
			</div>
		<?php
		if ( have_posts() ) :

			if ( is_home() && ! is_front_page() ) : ?>
				<div class="entry-header">
					<div class="container">
						<h2>Programmation</h2>
					</div>
				</div><!-- .entry-header -->
			<?php
			endif; ?>

			<div class="container">
				<div class="row">
					<div class="row">
						<div class="col s12">
							<form action="<?php bloginfo('url'); ?>/" method="get">
								<?php
								$select = wp_dropdown_categories('show_option_all=Tous les spectacles&show_count=1&orderby=name&echo=0&selected=0');
								$select = preg_replace("#<select([^>]*)>#", "<select$1 onchange='return this.form.submit()'>", $select);
								echo $select;
								?>
								<noscript><input type="submit" value="View"/></noscript>
							</form>
						</div>
					</div>
				</div>
				<div class="row no-row">
					<div class="accueil_spectacles">
						<?php
						/* Start the Loop */
						while ( have_posts() ) : the_post();

							get_template_part( 'template-parts/content', 'card' );

						endwhile;
						
						else :

							get_template_part( 'template-parts/content', 'none' );

						endif; ?>
					</div>
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
