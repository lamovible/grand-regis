<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package grand-regis
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'grand-regis' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
        <?php if (is_front_page()) : ?>
        <div class="slider">
            <div class="overlay"></div>
            <div class="bandeau">
                <div class="wrapper">
                    <object type="image/svg+xml" data="<?php echo get_template_directory_uri(); ?>/img/logo_white.svg">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/logo_white.png" alt="logo du Grand Régis">
                    </object>
                </div>
            </div>
            <div id="owl-example" class="owl-carousel">
                <?php
                $args = array(
                    'post_type' => 'spectacle',
                    'posts_per_page' => 8,
                    'orderby' => 'meta_value_num',
                    'meta_key' => 'prixSpectacle',
                    'order' => 'ASC');

                $spectaclesMajeurs = new WP_Query($args);
                wp_reset_postdata();
                wp_reset_query();
                if ($spectaclesMajeurs->have_posts()) {
                    while ($spectaclesMajeurs->have_posts()) : $spectaclesMajeurs->the_post();
                        the_post_thumbnail();
                    endwhile;
                }
                ?>
            </div>
            <?php else : ?>
        <div class="header_secondaire">
            <!--Insérer image secondaire ici-->
            <div class="wrapper">
                <?php
                if (is_page('Contact')) {
                    $location = get_field('carte');
                    if( !empty($location) ):
                        ?>
                        <div class="acf-map">
                            <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
                        </div>
                    <?php endif;
                }
                else {
                    if ( has_post_thumbnail() ) {
                        the_post_thumbnail();
                    }
                } ?>
            <?php endif; ?>
            </div>
            <nav>
                <div class="container">
                    <div class="nav-wrapper">
                        <a href="<?php echo esc_url(home_url('/')); ?>" rel="home" class="brand-logo">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/logo_white.png" alt="logo du Grand Régis">
                        </a>
                        <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
                        <div class="right hide-on-med-and-down">
                            <?php wp_nav_menu(array('theme_location' => 'primary', 'menu_id' => 'primary-menu')); ?>
                        </div>
                        <ul class="side-nav" id="mobile-demo">
                            <?php wp_nav_menu(array('theme_location' => 'primary', 'menu_id' => 'primary-menu')); ?>
                        </ul>
                    </div>
                </div>
            </nav>
        </div><!-- .slider ou .header_secondaire selon le type de page-->
	</header><!-- #masthead -->

	<div id="content" class="site-content">
