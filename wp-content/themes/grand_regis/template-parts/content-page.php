<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package grand_regis
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-header">
		<div class="container">
			<?php the_title('<h2 class="entry-title">', '</h2>'); ?>
		</div>
	</div><!-- .entry-header -->
	<div class="container">
		<div class="entry-content">
			<?php
			the_content();

			wp_link_pages(array(
				'before' => '<div class="page-links">' . esc_html__('Pages:', 'grand_regis'),
				'after' => '</div>',
			));
			?>
		</div><!-- .entry-content -->

		<?php if (get_edit_post_link()) : ?>
			<div class="entry-footer">
				<?php
				edit_post_link(
					sprintf(
					/* translators: %s: Name of current post */
						esc_html__('Edit %s', 'grand_regis'),
						the_title('<span class="screen-reader-text">"', '"</span>', false)
					),
					'<span class="edit-link">',
					'</span>'
				);
				?>
			</div><!-- .entry-footer -->
		<?php endif; ?>
	</div>
</article><!-- #post-## -->


