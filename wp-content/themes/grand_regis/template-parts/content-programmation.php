<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package grand_regis
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-header">
		<div class="container">
			<?php the_title('<h2 class="entry-title">', '</h2>'); ?>
		</div>
	</div><!-- .entry-header -->
	<div class="container">
		<div class="row">
            <div class="col s12">
                <form action="<?php bloginfo('url'); ?>/" method="get">
                    <?php
                    $select = wp_dropdown_categories('show_option_all=Tous les spectacles&show_count=1&orderby=name&echo=0&selected=0');
                    $select = preg_replace("#<select([^>]*)>#", "<select$1 onchange='return this.form.submit()'>", $select);
                    echo $select;
                    ?>
                    <noscript><input type="submit" value="View"/></noscript>
                </form>
            </div>
        </div>
        <div class="row no-row">
            <div class="accueil_spectacles">
                <?php
                $args = array(
                    'post_type' => 'spectacle',
                    'orderby' => 'meta_value',
                    'meta_key' => 'dateSpectacle',
                    'posts_per_page' => -1,
                    'order' => 'ASC');
                $prochainsSpectacles = new WP_Query($args);
                wp_reset_postdata();
                wp_reset_query();
                if ($prochainsSpectacles->have_posts()) {
                    while ($prochainsSpectacles->have_posts()) : $prochainsSpectacles->the_post();
                        get_template_part( 'template-parts/content', 'card' );
                    endwhile;
                }
                ?>
            </div>
        </div>
				<?php
				wp_link_pages(array(
					'before' => '<div class="page-links">' . esc_html__('Pages:', 'grand_regis'),
					'after' => '</div>',
				));
				?>
			</div><!-- .contact-content -->
		</div>
		<?php if (get_edit_post_link()) : ?>
			<div class="entry-footer">
				<?php
				edit_post_link(
					sprintf(
					/* translators: %s: Name of current post */
						esc_html__('Edit %s', 'grand_regis'),
						the_title('<span class="screen-reader-text">"', '"</span>', false)
					),
					'<span class="edit-link">',
					'</span>'
				);
				?>
			</div><!-- .entry-footer -->
		<?php endif; ?>
	</div>
</article><!-- #post-## -->


