<div class="col s6 m4 l3">
    <div class="card_artiste">
        <?php the_post_thumbnail(); ?>
        <div class="card_overlay">
            <div class="wrapper">
                <h3 class="card_info center">
                    <span class="card_titre"><?php the_title(); ?></span><br>
                    <div class="card_date">
                        <span class="card_jour"><?php
                            $date = get_post_meta(get_the_ID(), 'dateSpectacle', true);
                            $jour = substr($date, 8);
                            echo($jour);
                            ?></span><br>
                        <span class="card_mois"><?php
                            $date = get_post_meta(get_the_ID(), 'moisSpectacle', true);
                            $jour = substr($date, 0,3);
                            echo($jour);
                            ?></span><br>
                    </div>
                </h3>
                <a href="<?php echo(get_post_permalink()); ?>" class="waves-effect waves-light btn">acheter</a>
            </div>
        </div>
    </div>
</div>