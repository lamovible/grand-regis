<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package grand_regis
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-header">
		<div class="container">
			<?php the_title('<h2 class="entry-title">', '</h2>'); ?>
		</div>
	</div><!-- .entry-header -->
	<div class="container">
		<div class="row no-row row-contact">
			<div class="contact-content">
				<div class="col s12 m5 l4">
					<div class="card_contact">
						<div class="img_contact">
							<?php
							$image = get_field('img_contact');
							if( !empty($image) ): ?>
								<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							<?php endif; ?>
						</div>
						<div class="contact_info">
							<h3>Contactez-nous</h3>
							<?php the_content(); ?>
						</div>
					</div>
				</div>
				<div class="col s12 m7 l8">
					<div class="card_contact">
						<div class="wrapper">
							<?php
							if (function_exists('ninja_forms_display_form')) {
								ninja_forms_display_form(1);
							}
							?>
						</div>
					</div>
				</div>
				<?php

				wp_link_pages(array(
					'before' => '<div class="page-links">' . esc_html__('Pages:', 'grand_regis'),
					'after' => '</div>',
				));
				?>
			</div><!-- .contact-content -->
		</div>
		<?php if (get_edit_post_link()) : ?>
			<div class="entry-footer">
				<?php
				edit_post_link(
					sprintf(
					/* translators: %s: Name of current post */
						esc_html__('Edit %s', 'grand_regis'),
						the_title('<span class="screen-reader-text">"', '"</span>', false)
					),
					'<span class="edit-link">',
					'</span>'
				);
				?>
			</div><!-- .entry-footer -->
		<?php endif; ?>
	</div>
</article><!-- #post-## -->


