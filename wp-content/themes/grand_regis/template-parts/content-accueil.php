<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package grand_regis
 */

?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="entry-header">
            <div class="container">
                <h2>consulter les spectacles à venir</h2>
            </div>
        </div><!-- .entry-header -->
            <div class="container">
                <div class="entry-content">
                    <?php
                    the_content();

                    wp_link_pages(array(
                        'before' => '<div class="page-links">' . esc_html__('Pages:', 'grand_regis'),
                        'after' => '</div>',
                    ));
                    ?>
                </div><!-- .entry-content -->
                <div class="row no-row">
                    <div class="accueil_spectacles">

                        <?php
                        $args = array(
                            'post_type' => 'spectacle',
                            'orderby' => 'meta_value',
                            'meta_key' => 'dateSpectacle',
                            'posts_per_page' => 10,
                            'order' => 'ASC');
                        $prochainsSpectacles = new WP_Query($args);
                        wp_reset_postdata();
                        wp_reset_query();
                        if ($prochainsSpectacles->have_posts()) {
                            while ($prochainsSpectacles->have_posts()) : $prochainsSpectacles->the_post();
                                get_template_part( 'template-parts/content', 'card' );
                            endwhile;
                        }
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="center voir_plus">
                        <a href="#">En voir plus</a>
                    </div>
                </div>
                <?php if (get_edit_post_link()) : ?>
                    <div class="entry-footer">
                        <?php
                        edit_post_link(
                            sprintf(
                            /* translators: %s: Name of current post */
                                esc_html__('Edit %s', 'grand_regis'),
                                the_title('<span class="screen-reader-text">"', '"</span>', false)
                            ),
                            '<span class="edit-link">',
                            '</span>'
                        );
                        ?>
                    </div><!-- .entry-footer -->
                <?php endif; ?>
            </div>
	</article><!-- #post-## -->


