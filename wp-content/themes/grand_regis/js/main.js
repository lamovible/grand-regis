$( document ).ready(function() {

    if ($('#owl-example').length != 0) {
        //::::::::::::Carousel::::::::::::
        $("#owl-example").owlCarousel({
            navigation:true,
            navigationText: [
                "<span class='fa fa-chevron-left'></span>",
                "<span class='fa fa-chevron-right'></span>"
            ],
            slideSpeed : 300,
            paginationSpeed : 400,
            singleItem:true,
            autoPlay: true,
            pagination: false
        });
    }
    //::::::::::::Nav::::::::::::
    $(".button-collapse").sideNav();

    var distance = $('nav').offset().top,
        $window = $(window);

    $window.scroll(function() {
        if ( $window.scrollTop() >= distance ) {
            $('nav').addClass('nav_fixe');
        }
        else if ( $window.scrollTop() >= 0 ) {
            $('nav').removeClass('nav_fixe');
        }
    });

    //::::::::::::Select::::::::::::
    $('select').material_select();

    //::::::::::::Div amovibles::::::::::::
    var lastScrollTop = 0;
    $(window).scroll(function(event) {

        var st = $(this).scrollTop();

        $('.accueil_spectacles div:nth-of-type(2n+1) .card_artiste, .contact-content div:nth-of-type(2n+1) .card_contact').each(function () {

            var distanceCard = $(this).offset().top;
            if ($(window).scrollTop() + $(window).height() >= distanceCard + 237.5) {

                var margin = parseFloat($(this).css('margin-top'));
                if (margin < 30 && st > lastScrollTop) {
                    $(this).css('margin-top', ++margin);
                }
                else if (margin > -30 && st < lastScrollTop) {
                    $(this).css('margin-top', --margin);
                }
            }
        });

        $('.accueil_spectacles div:nth-of-type(2n) .card_artiste, .contact-content div:nth-of-type(2n) .card_contact').each(function () {

            var distanceCard = $(this).offset().top;
            if ($(window).scrollTop() + $(window).height() >= distanceCard + 237.5) {

                var margin = parseFloat($(this).css('margin-top'));
                if (margin > -30 && st > lastScrollTop) {
                    $(this).css('margin-top', --margin);
                }
                else if (margin < 30 && st < lastScrollTop) {
                    $(this).css('margin-top', ++margin);
                }
            }
        });
        lastScrollTop = st;
    });

});