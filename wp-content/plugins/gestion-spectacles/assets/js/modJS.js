var tps=0.05, tvq=0.09975, fraisEnvoi =0, totalbillet;

(function($) {
$(document).ready(function(){
console.log($('#post_id').val());
    $(".frais").hide();
    $(".envoiPoste").hide();
    $(".evoiEmail").hide();
    $("#calculFraiEnvoi").hide();
    $("#envoiPosteConfirmer").hide();
    $("#envoiCourrielConfirmer").hide();

    if (!$(".entry-content").hasClass("container"))
      {
        $(".entry-content").addClass("container");
      };

    var minOffset = 0, maxOffset = 4; // Change to whatever you want
    var thisYear = (new Date()).getFullYear();
    var select = $('#cDate');
    for (var i = minOffset; i <= maxOffset; i++) {
        var year = thisYear + i;
        $('<option>', {value: year, text: year}).appendTo(select);
    }
    for (var x = 1; x <= 12; x++) {

        $('<option>', {value: x, text: x}).appendTo($('#cMois'));
    }

        $( ".livraison" ).change(function() {

      		if($("#livraison").val()==="poste"){
      			$(".frais").show();
      			$(".envoiPoste").show()
        		$(".evoiEmail").hide()
      		}
      		else{
      			$(".frais").hide()
      			$(".envoiPoste").hide()
        		$(".evoiEmail").show()
      		}
    	});
      $("#quantiteBillet").change(function(){

      totalbillet = $('#prixBiilet').text() * $("#quantiteBillet").val();

      var tpst = totalbillet*tps;
      var tvqt = totalbillet*tvq;

      $('#totalbilletA').text(totalbillet);
      totalbillet = totalbillet + tpst + tvqt + fraisEnvoi;
      $("#tps").text(tpst.toFixed(2));
      $("#tvq").text(tvqt.toFixed(2));
      $('#total').text(totalbillet.toFixed(2));
      });

      //confirmation des billet:
      $('#achatBillet').on('submit', function(e){
        e.preventDefault();
        var billet = 1;
        if (confirtmationBillet()===true) {
          var   data={
              'action': 'my_action',
              'post_id': $('#post_id').val(),
              'totalBilletAchat': billet
            }
          jQuery.post(my_ajax_url.ajax_url, data);
        }
      });
  });
 })( jQuery );


 document.getElementById("etape_deux_").addEventListener("click", alleretapeInfo, false);
 document.getElementById("etape_trois").addEventListener("click", validerInfoPersonnelle, false);
 document.getElementById("defaultOpen").addEventListener("click", etapeSuivante(event, 'defaultOpen', 'infoSpectacle'), false);


document.getElementById("defaultOpen").click();
function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" activer", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " activer";
}
function etapeSuivante(evt, tab, etapeNom) {
    // Declare les variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // change la classe active de tout les element
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" activer", "");
    }

    // affiche la tab correspondent
    document.getElementById(etapeNom).style.display = "block";
    document.getElementById(tab).className += " activer";

}
function alleretapeInfo(){

  etapeSuivante(event, 'tab_etape_deux', 'infoPerso');

}
function validerInfoPersonnelle(){
  var valider = true;
  var nomComplet="";

  //exprecion regulieres:
  var emailValidate = /^[^\W][a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\@[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\.[a-zA-Z]{2,4}$/
  var cPRegex = /^[A-Za-z]\d[A-Za-z][ -]?\d[A-Za-z]\d$/;
  var telRegex= /^(\()?[2-9]{1}\d{2}(\))?(-|\s)?\d{3}(-|\s)\d{4}$/;


  //validation du nom
  if (document.getElementById('nom').value.trim() == "") {
    document.getElementById('nom').style.borderBottomColor  = "red";
    document.getElementById("nom").style.boxShadow="0 1px 0 0 #FA8072";
    valider = false;
  }
  else{
    document.getElementById('nom').style.borderBottomColor  = "inherit";
    document.getElementById("nom").style.boxShadow="inherit";
    nomComplet +=  " " + document.getElementById('nom').value.trim();
  }

  //validation prenom
  if (document.getElementById('prenom').value.trim() == "") {
    document.getElementById('prenom').style.borderBottomColor  = "red";
    document.getElementById('prenom').style.backgroundColor  = "#FA8072";
    document.getElementById("prenom").style.boxShadow="0 1px 0 0 #FA8072";
    valider = false;
  }
  else{
    document.getElementById('prenom').style.borderBottomColor  = "inherit";
    document.getElementById('prenom').style.backgroundColor  = "inherit";
    document.getElementById("prenom").style.boxShadow="inherit";
    nomComplet +=  " " + document.getElementById('prenom').value.trim();
  }

  //validation du curriel ou de l'adresse selon le choix
  if (document.getElementById('livraison').value == 1) {
    document.getElementById('livraison').style.backgroundColor  = "#FA8072";
    valider = false;
  }
  else if (document.getElementById('livraison').value == "poste") {
    var telephone = document.getElementById('telephone').value.trim()
    telephone = telephone.replace(/[^\d]/g,'')
    document.getElementById('livraison').style.backgroundColor  = "inherit";
    //validation adresse
    if (document.getElementById('adresse').value.trim() == "") {
      document.getElementById('adresse').style.borderBottomColor  = "red";
      document.getElementById('adresse').style.backgroundColor  = "#FA8072";
      document.getElementById("adresse").style.boxShadow="0 1px 0 0 #FA8072";
      valider = false;
    }
    else{
      document.getElementById('adresse').style.borderBottomColor  = "inherit";
      document.getElementById('adresse').style.backgroundColor  = "inherit";
      document.getElementById("adresse").style.boxShadow="inherit";
    }
    //validation codePostale
    if (document.getElementById('codePostale').value.trim() == "") {
      document.getElementById('codePostale').style.borderBottomColor  = "red";
      document.getElementById('codePostale').style.backgroundColor  = "#FA8072";
      document.getElementById("codePostale").style.boxShadow="0 1px 0 0 #FA8072";
    }
    else if (!cPRegex.test(document.getElementById('codePostale').value.trim())) {
      document.getElementById('codePostale').style.borderBottomColor  = "red";
      document.getElementById('codePostale').style.backgroundColor  = "#FA8072";
      document.getElementById("codePostale").style.boxShadow="0 1px 0 0 #FA8072";
      valider = false;
    }
    else{
      document.getElementById('codePostale').style.borderBottomColor  = "inherit";
      document.getElementById('codePostale').style.backgroundColor  = "inherit";
      document.getElementById("nom").style.boxShadow="inherit";
    }
    //validation ville
    if (document.getElementById('ville').value.trim() == "") {
      document.getElementById('ville').style.borderBottomColor  = "red";
      document.getElementById('ville').style.backgroundColor  = "#FA8072";
      document.getElementById("ville").style.boxShadow="0 1px 0 0 #FA8072";
      valider = false;
    }
    else{
      document.getElementById('ville').style.borderBottomColor  = "inherit";
      document.getElementById('ville').style.backgroundColor  = "inherit";
      document.getElementById('ville').style.boxShadow="inherit";
    }
    //validation telephone
    if (document.getElementById('telephone').value.trim() == "") {
      document.getElementById('telephone').style.borderBottomColor  = "red";
      document.getElementById('telephone').style.backgroundColor  = "#FA8072";
      document.getElementById("telephone").style.boxShadow="0 1px 0 0 #FA8072";
      valider = false;
    }
    else if (telephone.length !=10) {
      document.getElementById('telephone').style.borderBottomColor  = "red";
      document.getElementById('telephone').style.backgroundColor  = "#FA8072";
      document.getElementById("telephone").style.boxShadow="0 1px 0 0 #FA8072"
      valider = false;
    }
    else{
      document.getElementById('telephone').style.borderBottomColor  = "inherit";
      document.getElementById('telephone').style.backgroundColor  = "inherit";
      document.getElementById("telephone").style.boxShadow="inherit";
    }
    //validation courriel
    if (document.getElementById('courriel').value.trim() == "") {
      document.getElementById('courriel').style.borderBottomColor  = "red";
      document.getElementById('courriel').style.backgroundColor  = "#FA8072";
      document.getElementById("courriel").style.boxShadow="0 1px 0 0 #FA8072"
      valider = false;
    }
    else if (!emailValidate.test(document.getElementById('courriel').value.trim())) {
      document.getElementById('courriel').style.borderBottomColor  = "red";
      document.getElementById('courriel').style.backgroundColor  = "#FA8072";
      document.getElementById("courriel").style.boxShadow="0 1px 0 0 #FA8072"
      valider = false;
    }
    else{
      document.getElementById('courriel').style.borderBottomColor  = "inherit";
      document.getElementById('courriel').style.backgroundColor  = "inherit";
      document.getElementById("courriel").style.boxShadow="inherit";
    }
    //validation province
    if (document.getElementById('province').value.trim() == "") {
      document.getElementById('province').style.borderBottomColor  = "red";
      document.getElementById('province').style.backgroundColor  = "#FA8072";
      document.getElementById("province").style.boxShadow="0 1px 0 0 #FA8072";
      valider = false;
    }
    else{
      document.getElementById('province').style.borderBottomColor  = "inherit";
      document.getElementById('province').style.backgroundColor  = "inherit";
      document.getElementById("province").style.boxShadow="inherit";
    }
  }
  else{
      var courrielE = document.getElementById('courrielE').value.trim();
      if (document.getElementById('courrielE').value.trim() == "") {
        document.getElementById('courrielE').style.borderBottomColor  = "red";
        document.getElementById('courrielE').style.backgroundColor  = "#FA8072";
        document.getElementById("courrielE").style.boxShadow="0 1px 0 0 #FA8072"
        valider = false;
      }
      else if (!emailValidate.test(courrielE)) {
        document.getElementById('courrielE').style.borderBottomColor  = "red";
        document.getElementById('courrielE').style.backgroundColor  = "#FA8072";
        document.getElementById("courrielE").style.boxShadow="0 1px 0 0 #FA8072"
        valider = false;
      }
      else{
        document.getElementById('courriel').style.borderBottomColor  = "inherit";
        document.getElementById('courriel').style.backgroundColor  = "inherit";
        document.getElementById("courriel").style.boxShadow="inherit";
      }
  }

  if(valider == true){
    if (document.getElementById('livraison').value == "poste") {
      document.getElementById('calculFraiEnvoi').style.diplay="block";
      document.getElementById('fraisEnvoi').innerHTML = fraisEnvoi;
      document.getElementById('infoDestinataireC').innerHTML = nomComplet;
      document.getElementById('confirmAdresse').innerHTML = document.getElementById('adresse').value.trim();
      document.getElementById('confirmCP').innerHTML = document.getElementById('codePostale').value.trim();
      document.getElementById('confirmVille').innerHTML = document.getElementById('ville').value.trim();
      document.getElementById('confirmProvincr').innerHTML = document.getElementById('province').value.trim();
      document.getElementById('confirmEmail').innerHTML = document.getElementById('courriel').value.trim();
      document.getElementById('envoiPosteConfirmer').style.display="block";
      document.getElementById('validayorPhP').value = 1;

    }
    else{
      document.getElementById('infoDestinataireC').innerHTML = nomComplet;
      document.getElementById('confirmeCouriel').innerHTML = document.getElementById('courrielE').value.trim();
      document.getElementById('envoiCourrielConfirmer').style.display ="block";
      document.getElementById('validayorPhP').value = 1;
    }
    etapeSuivante(event, 'tab_etape_trois', 'metPei')
  }
}

function confirtmationBillet(e){
  var valider = true;
  var caseCocher = false;
  var prix = document.getElementById('prixBiilet').innerHTML;
  var cvcRegex = /^[0-9]{3,4}$/;
  var carteStr = document.getElementById("carte").value.trim();
  carteStr = carteStr.replace(/ /g, "");
  //verifie la quantite de billet et fai
  if(document.getElementById('quantiteBillet').value == 0 || document.getElementById('quantiteBillet').value <=0){
    document.getElementById('quantiteBillet').style.borderBottomColor  = "red";
    document.getElementById('quantiteBillet').style.backgroundColor  = "#FA8072";
    document.getElementById("quantiteBillet").style.boxShadow="0 1px 0 0 #FA8072";
    valider = false;
  }
  else{
    document.getElementById('quantiteBillet').style.borderBottomColor  = "inherit";
    document.getElementById('quantiteBillet').style.backgroundColor  = "inherit";
    document.getElementById("quantiteBillet").style.boxShadow="inherit";

  }

  //verification des carte de credit
  if (document.getElementById('carte').value=="") {
   document.getElementById('carte').style.borderBottomColor  = "red";
   document.getElementById('carte').style.backgroundColor  = "#FA8072";
   document.getElementById("carte").style.boxShadow="0 1px 0 0 #FA8072";
   valider= false;
  }
  else {
   document.getElementById('carte').style.borderBottomColor  = "inherit";
   document.getElementById('carte').style.backgroundColor  = "inherit";
   document.getElementById("carte").style.boxShadow="inherit";
  }

  for (var i=0; i<achatBillet.carteCredit.length; i++) {
    if (achatBillet.carteCredit[i].checked) {
      var carte = achatBillet.carteCredit[i].value;
      if (carte == "visa") {
        caseCocher= true;
        if (carteStr.substr(0, 4) == "5258" || carteStr.substr(0, 4) != "4540") {
          document.getElementById('carte').style.borderBottomColor  = "red";
          document.getElementById('carte').style.backgroundColor  = "#FA8072";
          document.getElementById("carte").style.boxShadow="0 1px 0 0 #FA8072";
          valider = false;
        }
        else if (carteStr == "" || carteStr.length == 0) {
          document.getElementById('carte').style.borderBottomColor  = "red";
          document.getElementById('carte').style.backgroundColor  = "#FA8072";
          document.getElementById("carte").style.boxShadow="0 1px 0 0 #FA8072";
          valider= false;
        }
        else if (carteStr.length>16) {
          document.getElementById('carte').style.borderBottomColor  = "red";
          document.getElementById('carte').style.backgroundColor  = "#FA8072";
          document.getElementById("carte").style.boxShadow="0 1px 0 0 #FA8072";
          valider = false;
        }
        else{
          document.getElementById('carte').style.borderBottomColor  = "inherit";
          document.getElementById('carte').style.backgroundColor  = "inherit";
          document.getElementById("carte").style.boxShadow="inherit";
        }
      }
      //validation si ce mastercard
      else if(carte == "mastercard") {
        caseCocher = true;
        if (carteStr.substr(0, 4) == "4540" || carteStr.substr(0, 4) != "5258") {
          document.getElementById('carte').style.borderBottomColor  = "red";
          document.getElementById('carte').style.backgroundColor  = "#FA8072";
          document.getElementById("carte").style.boxShadow="0 1px 0 0 #FA8072";
          valider = false;
        }
        else if (carteStr == "" || carteStr.length == 0) {
          document.getElementById('carte').style.borderBottomColor  = "red";
          document.getElementById('carte').style.backgroundColor  = "#FA8072";
          document.getElementById("carte").style.boxShadow="0 1px 0 0 #FA8072";
          valider= false;
        }
        else if (carteStr.length>16) {
          document.getElementById('carte').style.borderBottomColor  = "red";
          document.getElementById('carte').style.backgroundColor  = "#FA8072";
          document.getElementById("carte").style.boxShadow="0 1px 0 0 #FA8072";
          valider = false;
        }
        else{
          document.getElementById('carte').style.borderBottomColor  = "inherit";
          document.getElementById('carte').style.backgroundColor  = "inherit";
          document.getElementById("carte").style.boxShadow="inherit";
        }
      }
    }
  }
 //valide si un choix de carte de credit as etait choisi
   if (caseCocher == false) {
     document.getElementById("messageErrorCarte").style.color = "red";
     document.getElementById("messageErrorCarte").innerHTML = "Un type de carte doit etre choisi";
     validar = false;
   }
   else{
     document.getElementById("messageErrorCarte").style.color = "inherit";
     document.getElementById("messageErrorCarte").innerHTML = "Selectionner un type de carte" ;
   }
   //valide le champ CVC
  if (!cvcRegex.test(document.getElementById('cvc').value) || document.getElementById('cvc').value=="") {
     document.getElementById('cvc').style.borderBottomColor  = "red";
     document.getElementById('cvc').style.backgroundColor  = "#FA8072";
     document.getElementById("cvc").style.boxShadow="0 1px 0 0 #FA8072";
     valider= false;
  }
  else {
     document.getElementById('cvc').style.borderBottomColor  = "inherit";
     document.getElementById('cvc').style.backgroundColor  = "inherit";
     document.getElementById("cvc").style.boxShadow="inherit";
  }

  if(valider==false){
    return false;
  }
  else{

    etapeSuivante(event, 'confirmationdAchat', 'confirAchat');etapeSuivante(event, 'confirmationdAchat', 'confirAchat');
    return true;

  }
}
