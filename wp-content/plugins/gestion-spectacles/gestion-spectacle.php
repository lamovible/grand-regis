<?php 
/**
 * Plugin Name: Gestion Spectacles Projet Production 2
 * Plugin URI: 
 * Description: Extension qui gère les spectacles (contenu et processus d'achat).
 * Version:     1.0.0
 * Author:      Equipe Jerome Vanessa Junior KomiS
 */


if (! defined('WPINC')) die;

//ajout des classes
require_once plugin_dir_path( __FILE__ ) . 'includes/class-gestion-spectacles.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/class-gestion-spectacles-admin.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/class-gestion-spectacle-display.php';

function start_gestion_spectacle() 
{
	
	$admin = new Gestion_Spectacles_Admin();
	$plugin = new Gestion_Spectacles( $admin );
	$display = new Gestion_spectacles_Display();

	$plugin->initialize();
	$display->initialize();
	$admin->initialize();
}

start_gestion_spectacle();


















 ?>