<?php
class Gestion_spectacles_Display {

	public function initialize() {
        add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_styles' ), 11 );
        add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_script' ) );

				add_action( 'wp_ajax_my_action',  array( $this, 'my_action' ));
				add_action( 'wp_ajax_nopriv_my_action', array( $this, 'my_action' ));

				add_filter( 'the_content', array( $this, 'display_mes_valeurs' ) );
				add_shortcode( 'mon_shortcode', array( $this, 'mon_shortcode_function' ));

	}

	public function display_mes_valeurs( $content ) {
		if(get_post_type()=="spectacle"){
			echo $content;
			 require_once( plugin_dir_path( __FILE__ ) . '../views/gestion-spectacles-display1.php');
		}
		else{
			return $content;
		}


	}
	public function enqueue_script(){
		//wp_enqueue_script("jquery");

		if (get_post_type() == 'spectacle') {
			wp_enqueue_script(

				'gestion-spectacle-js',
				plugins_url('gestion-spectacles/assets/js/modJS.js'),
				array("jquery"),
				'1.0.0',
				true
			);
		}


		wp_localize_script('gestion-spectacle-js','my_ajax_url', array(
			'ajax_url' => admin_url( 'admin-ajax.php' )
		));
	}

	function my_action(){
		$post_id = intval($_POST['post_id']);
		$place = get_post_meta($post_id, 'placeSpectacle', true);
		$nouvellePLaces = $place-$_POST['totalBilletAchat'];
		update_post_meta( $post_id, 'placeSpectacle', $nouvellePLaces );
		wp_die();
	}

	public function enqueue_styles() {

		wp_enqueue_style(
			'gestion-spectacle-style',
			plugins_url('gestion-spectacles/assets/css/monStyle.css'),
			'1.0.0'
		);

	}

    public function mon_shortcode_function() {
		require_once( plugin_dir_path( __FILE__ ) . '../views/display-etape1.php');
	}
}
 ?>
