<?php  
class Gestion_Spectacles {

	public function __construct( $admin ) {
	$admin-> initialize();
	}
		
	public function initialize() {
	add_action( 'admin_enqueue_scripts', array( $this,  'charger_css_admin' ));
	add_action( 'admin_enqueue_scripts', array( $this,  'charger_js_admin' ));
	//add_action( 'ajout_menu');
	add_action( 'init', array( $this, 'creer_post_type' ) );

		add_action( 'pre_get_posts', function ( $q ) {
			if (    !is_admin() // Only target front end queries
				&& $q->is_main_query() // Only target the main query
				&& $q->is_category() // Only target category archives
			) {
				$q->set( 'post_type', ['post', 'custom_post_type'] ); // Adjust as needed
			}
		});


	}

	public function charger_css_admin() {
		wp_enqueue_style( 'monCSS', plugins_url( '../assets/css/admin.css', __FILE__ ) );
		
	}
	public function charger_js_admin() {
		wp_enqueue_style( 'monJS', plugins_url( '../assets/js/admin.js', __FILE__ ) );
	   	//wp_enqueue_script('code_admin_plugin', plugin_dir_path( __FILE__ ) .'assets/js/admin.js');
	}

	public function ajout_menu(){
			add_menu_page("Gestion Spectacles", "Gestion Spectacles", 4, "example-options", "exampleMenu");
			add_submenu_page("example-options", "Option 1", "Option 1", 4, "example-options-1", "option1");
	}

	public function creer_post_type() 
	{
		 $labels = array(
	        'name'               => 'Spectacle',
	        'singular_name'      => 'Spectacle',
	        'menu_name'          => 'Spectacle',
	        'name_admin_bar'     => 'Spectacle options',
	        'add_new'            => 'Ajouter un spectacle',
	        'add_new_item'       => 'Ajouter un spectacle',
	        'new_item'           => 'Nouveau spectacle',
	        'edit_item'          => 'Modifier spectacles',
	        'view_item'          => 'Afficher spectacles',
	        'all_items'          => 'Tout les spectacles',
	        'search_items'       => 'Rechercher un spectacle',
	        'not_found'          => 'Aucun spectacle trouvé',
	    );
	 
	    $args = array( 
	        'public'      => true, 
	        'labels'      => $labels,
	        'rewrite'       => array( 'slug' => 'spectale' ),
	        'has_archive'   => true,
	        'menu_position' => 20,
	        'menu_icon'     => 'dashicons-tickets-alt',
	        'taxonomies'        => array( 'post_tag', 'category' ),
	        'supports'      => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
	    );
	        register_post_type( 'spectacle', $args );
	}



}

?>