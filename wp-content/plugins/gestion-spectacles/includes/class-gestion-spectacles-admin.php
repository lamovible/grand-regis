<?php 
class Gestion_Spectacles_Admin {

	public function initialize() {
		//add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_styles' ) );
		add_action( 'admin_init', array( $this, 'ajouter_meta_box' ) );
		add_action( 'save_post', array( $this, 'save_ma_publication' ) );
	}

	public function ajouter_meta_box() {

		add_meta_box(
			'meta-box-description-ma-publication',
			'Information de le spectacle',
			array( $this, 'view_entree_donnees_admin' ),
			'spectacle'
		);


	}
	/*public function enqueue_styles() {
		wp_enqueue_style(
			'gestion-spectacle-notice-admin',
			plugins_url('gestion-spectacles/assets/css/admin.css'),
			array(),
			'1.0.0'
		);
	}*/

	public function view_entree_donnees_admin() {
	      require_once( plugin_dir_path( __FILE__ ) . '../views/gestion-spectacles-admin.php');
	}

	public function save_ma_publication( $post_id ) {
		$prixSpectacle = $_POST[ 'prixSpectacle' ];  ///manque le nom du champs moisSpectacle
		$prixReserver = $_POST[ 'prixReservation' ];
		$placeSpectacle = $_POST[ 'placeSpectacle' ];
		$moisSpectacle = $_POST[ 'moisSpectacle' ];
		$date = $_POST[ 'dateSpectacle' ];
		$heures = $_POST[ 'heureSpectacle' ];
		$site_Artiste =$_POST[ 'siteArtiste' ];
		$facebook = $_POST[ 'facebookArtiste' ];
		$youtube = $_POST[ 'youtubeArtiste' ];
		$twitter = $_POST[ 'twitterArtiste' ];
		update_post_meta( $post_id, 'prixSpectacle', $prixSpectacle );
		update_post_meta( $post_id, 'prixReservation', $prixReserver );
		update_post_meta( $post_id, 'placeSpectacle', $placeSpectacle );
		update_post_meta( $post_id, 'dateSpectacle', $date );
		update_post_meta( $post_id, 'moisSpectacle', $moisSpectacle );
		update_post_meta( $post_id, 'heureSpectacle', $heures );
		update_post_meta( $post_id, 'siteArtiste', $site_Artiste );
		update_post_meta( $post_id, 'facebookArtiste', $facebook );
		update_post_meta( $post_id, 'youtubeArtiste', $youtube );
		update_post_meta( $post_id, 'twitterArtiste', $twitter );
	}

}

 ?>