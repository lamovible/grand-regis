<?php
    $prix = get_post_meta(get_the_ID(), 'prixSpectacle');
 ?>
    <main>
      <!--tabs-->
      <div class="row">
        <div class="col s12">
          <ul class="tab">
            <li><a href="javascript:void(0)" class="tablinks" id="defaultOpen">Spectacle</a></li>
            <li><a href="javascript:void(0)" class="tablinks"id="tab_etape_deux">Etape 1</a></li>
            <li><a href="javascript:void(0)" class="tablinks"  id="tab_etape_trois">Etape 2</a></li>
            <li><a href="javascript:void(0)" class="tablinks" id="confirmationdAchat">Confirmation</a></li>
          </ul>
        </div>


        <div id="infoSpectacle" class="tabcontent col s12">
            <?php echo get_post_meta(get_the_ID(), 'placeSpectacle', true); ?>
          <div class="row">
            <div class="col s12 m6 l4">
              <h2 class="nomArtiste">Un nom <!--the_title--></h2>
              <h3 class="dateSpectacle">date <!--date--></h3>
              <p class="invites">les inviter si il existe <!--inviter--></p>
              <div class="detailPrix">
                <p class="prixParterre"> Parterre: <span id="prixBiilet"><?php echo $prix[0]; ?></span>$</p>
                <p class="reserver"> Sièges rèservés:  <?php echo get_post_meta(get_the_ID(), 'prixReservation', true); ?>$</p>
                <button class="btn waves-effect waves-light" id="etape_deux_" >Acheter</button>
              </div>
            </div>

            <div class="col s12 m6 l8">
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati vel facere assumenda illum ut voluptates repellat minus laborum quis laboriosam deleniti consequuntur, excepturi tenetur. Earum quasi vitae, voluptates odio officiis! <!--the_content--></p>
            </div>
          </div>
        </div>
        <form name="achatBillet"  id="achatBillet" action="achatConfirmation.php" method="post">
          <div id="infoPerso" class=" tabcontent col s12">
            <div class="row">
              <div class="col s12">
                <div class="row">
                  <div class="input-field col s6">
                    <input id="prenom" type="text" class="validate" name="prenom">
                    <label for="prenom">Prenom</label>
                  </div>
                  <div class="input-field col s6">
                    <input id="nom" type="text" class="validate" name="nom">
                    <label for="nom">Nom</label>
                  </div>
                </div>

                <!--Choix de livraison-->
                <div class="row">

                  <div class="col s6">
                    <label>Choix de livraison</label>
                    <select class="livraison browser-default" id="livraison" required name="livraison">
                      <option value="1" disabled selected>Selectioner une option</option>
                      <option value="poste">Livré par la poste</option>
                      <option value="electronique">Livraison électronique</option>
                    </select>
                  </div>
                  <div class="col s6 frais">
                    <p>Des frais d'envoi de 8,70$ seront ajoutés pour l'envoi express.</p>
                  </div>
                </div>
                <!--information livraison-->
                <div class="row">
                 <div class="col s12 envoiPoste">
                  <div class="row">
                    <div class="input-field col s12">
                      <input id="adresse" type="text" class="validate" name="adresse">
                      <label for="prenom">Adresse civique</label>
                    </div>
                    <div class="input-field col s6">
                      <input id="codePostale" type="text" class="validate" name="codePostale">
                      <label for="codePostale">Code postal</label>
                    </div>
                    <div class="input-field col s6">
                      <input id="ville" type="text" class="validate" name="ville">
                      <label for="ville">Ville</label>
                    </div>
                    <div class="input-field col s6">
                      <input id="province" type="text" class="validate" name="province">
                      <label for="province">Province/territoire</label>
                    </div>
                    <div class="input-field col s6">
                      <input id="telephone" type="text" class="validate" name="telephone">
                      <label for="telephone">Numéro de téléphone</label>
                    </div>
                    <div class="input-field col s6">
                      <input id="courriel" type="text" class="validate" name="courriel">
                      <label for="courriel">Adresse de courriel</label>
                    </div>
                  </div>
                </div>
                <div class="col s12 evoiEmail">
                  <div class="row">
                    <div class="input-field col s6">
                      <input id="courrielE" type="text" class="validate" name="courrielE">
                      <label for="courriel">Adresse de courriel</label>
                    </div>
                  </div>
                </div>
              </div>
              <a class="btn waves-effect waves-light" id="etape_trois">Acheter</a>
            </div>
          </div>
          </div>
          <div id="metPei" class="tabcontent col s12">
            <!--divider-->
            <div class="row">
              <div class="input-fiel col s6">
                <label for="prenom">Quantite de billet</label>
                <input type="number" min="0" max="<?php echo get_post_meta(get_the_ID(), 'placeSpectacle', true);?>" id="quantiteBillet" class="validate" value="0" name="quantiteBillet">
                <input type="hidden" name="post_id" id="post_id" value=" <?php echo get_the_ID(); ?> ">
              </div>
            </div>
            <div class="row">
              <div class="col s12 m6 l6">
                <p >Methode de peiment</p>
                <p id="messageErrorCarte">Selectionner un type de carte</p>

                  <input name="carteCredit" type="radio" id="visa" value="visa">
                  <label for="visa"><img src="<?php echo plugin_dir_url( __FILE__) . '../assets/img/visa.png'; ?>" alt="visa"></label>
                </p>
                <p>
                  <input name="carteCredit" type="radio" id="mastercard" value="mastercard">
                  <label for="mastercard"><img src="<?php echo plugin_dir_url( __FILE__) . '../assets/img/mastercard.png';?>" alt="master card"></label>
                </p>
              </div>
              <div class="input-field col s12 m6 l6">
                <input id="carte" type="text" class="validate">
                <label for="carte">Numéro de carte</label>
              </div>
              <div class="input-field col s12 m6 l6">
                <input id="cvc" type="text" class="validate" maxlength="3">
                <label for="cvc">Code de vérification de la carte (CVC)</label>
              </div>
              <div class="row offset-m6 offset-l6">

                <div class="input-field col s4 s4 m2 l2 offset-m6 offset-l6">
                  <label for="cDate">Date d'expiration</label>
                </div>

                <div class="input-field col s4 s4 m2 l2">
                  <select name="cDate" id="cDate" class="browser-default"></select>
                </div>

                <div class="input-field col s4 m2 l2">
                  <select name="cMois" id="cMois" class="browser-default"></select>
                </div>
              </div>

            </div>
            <div class="divider"></div>
            <div class="col offset-m6 offset-l6 s12 m6 l6">
              <p>Total hors taxes: <span id="totalbilletA"></span>$</p>
              <p id="calculFraiEnvoi">Frais d'envoi il existent <span id="fraisEnvoi"></span>$</p>
              <div class="divider"></div>
              <p>TPS: <span id="tps"></span></p>
              <p>TVQ: <span id="tvq"></span></p>
              <p>Total: <span id="total"></span></p>
            </div>
            <div class="col s4">
              <button class="btn waves-effect waves-light" type="submit" name="action" id="confirmationAchat">Confirmer l'achat</button>
            </div>
          </div>

          <div id="confirAchat" class="tabcontent col s12">
            <div class="row">
                <div class="col s6">
                  <h3>Confirmation de l'achat</h3>
                  <p>
                    infotmation du destinataire:
                    <span id="infoDestinataireC"></span>
                  </p>

                </div>
                <div class="envoiPosteConfirmer col s12" id="envoiPosteConfirmer">
                  <h3>Le billet serais livre a l'adresse si desou:</h3>
                  <ul>
                    <li id="confirmAdresse"></li>
                    <li><span id="confirmCP"></span>, <span id="confirmVille"></span>, <span id="confirmProvincr"></span> </li>
                    <li>Un courriel de facturation serais envoyer a: <span id="confirmEmail"></span></li>
                  </ul>
                </div>
                <div class="envoiCourrielConfirmer col s12" id="envoiCourrielConfirmer">
                  <h3>Le billet vous serai envoyer a l'adresse courriel suivente: </h3>
                  <p id="confirmeCouriel">

                  </p>

                </div>
            </div>
            <input type="hidden" id="validayorPhP" value="0" />
            <?php
              update_post_meta( $post_id, 'placeSpectacle', $placeSpectacle );
             ?>
          </div>
        </form>
    </div>
  </main>
