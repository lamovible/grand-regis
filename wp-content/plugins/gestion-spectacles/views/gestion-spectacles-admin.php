
	 <div class=" container infoSpectacle">
 		<h4>les champs marquer par une * sont obligatoire</h4>
 		<div>
 			<label for="prixSpectacle">Prix Spectacle:<span class="obigatoire">*</span></label>
			<input class="col s12" type="text" name="prixSpectacle" value="<?php echo get_post_meta(get_the_ID(), 'prixSpectacle', true); ?>" required>
 		</div>
	 	<div>
	 		<label for="prixReservation">Sièges rèservés:<span class="obigatoire">*</span></label>
			<input type="text" name="prixReservation" value="<?php echo get_post_meta(get_the_ID(), 'prixReservation', true); ?>" required>
	 	</div>
	 	<div>
	 		<label for="placeSpectacle">Nombre de place<span class="obigatoire">*</span></label>
			<input type="number" name="placeSpectacle" value="<?php echo get_post_meta(get_the_ID(), 'placeSpectacle', true); ?>" min="0" max="500" required>
	 	</div>
	 	<div>
	 		<label for="dateSpectacle">Date spectacle:<span class="obigatoire">*</span></label>
			<input type="date" name="dateSpectacle" value="<?php echo get_post_meta(get_the_ID(), 'dateSpectacle', true); ?>" required>
	 	</div>
	 	<div>
	 		<label for="moisSpectacle">Mois:<span class="obigatoire">*</span></label>
	 		<select name="moisSpectacle" class="moisSpectacle" required>
			  <option value="Janvier" <?php if(get_post_meta(get_the_ID(), 'moisSpectacle', true) == "Janvier"){echo "selected=elected";} ?> >Janvier</option>
			  <option value="Fevrier" <?php if(get_post_meta(get_the_ID(), 'moisSpectacle', true) == "Fevrier"){echo "selected=elected";} ?> >Février</option>
			  <option value="Mars" <?php if(get_post_meta(get_the_ID(), 'moisSpectacle', true) == "Mars"){echo "selected=elected";} ?> >Mars</option>
			  <option value="Avril" <?php if(get_post_meta(get_the_ID(), 'moisSpectacle', true) == "Avril"){echo "selected=elected";} ?> >Avril</option>
			   <option value="Mai" <?php if(get_post_meta(get_the_ID(), 'moisSpectacle', true) == "Mai"){echo "selected=elected";} ?> >Mai</option>
			  <option value="Juin" <?php if(get_post_meta(get_the_ID(), 'moisSpectacle', true) == "Juin"){echo "selected=elected";} ?> >Juin</option>
			  <option value="Juillet" <?php if(get_post_meta(get_the_ID(), 'moisSpectacle', true) == "Juillet"){echo "selected=elected";} ?> >Juillet</option>
			  <option value="Aout" <?php if(get_post_meta(get_the_ID(), 'moisSpectacle', true) == "Aout"){echo "selected=elected";} ?> >Août</option>
			  <option value="Septembre" <?php if(get_post_meta(get_the_ID(), 'moisSpectacle', true) == "Septembre"){echo "selected=elected";} ?> >Septembre</option>
			   <option value="Octobre" <?php if(get_post_meta(get_the_ID(), 'moisSpectacle', true) == "Octobre"){echo "selected=elected";} ?> >Octobre</option>
			  <option value="Novembre" <?php if(get_post_meta(get_the_ID(), 'moisSpectacle', true) == "Novembre"){echo "selected=elected";} ?> >Novembre</option>
			   <option value="Decembre" <?php if(get_post_meta(get_the_ID(), 'moisSpectacle', true) == "Decembre"){echo "selected=elected";} ?> >Décembre</option>
			</select>
		
	 	</div>
	 	<div>
	 		<label for="heureSpectacle">Heure spectacle:<span class="obigatoire">*</span></label>
			<input type="text" name="heureSpectacle" value="<?php echo get_post_meta(get_the_ID(), 'heureSpectacle', true); ?>" required>
	 	</div>
	 	<div>
	 		<label for="siteArtiste">Site web de l'artiste:</label>
			<input type="text" name="siteArtiste" value="<?php echo get_post_meta(get_the_ID(), 'siteArtiste', true); ?>" required>
	 	</div>
	 	<div>
	 		<label for="facebookArtiste">Facebook:</label>
			<input type="text" name="facebookArtiste" value="<?php echo get_post_meta(get_the_ID(), 'facebookArtiste', true); ?>" required>
	 	</div>
	 	<div>
	 		<label for="youtubeArtiste">Chaine youtube:</label>
			<input type="text" name="youtubeArtiste" value="<?php echo get_post_meta(get_the_ID(), 'youtubeArtiste', true); ?>" required>
	 	</div>
	 	<div>
	 		<label for="twitterArtiste">Twitter:</label>
			<input type="text" name="twitterArtiste" value="<?php echo get_post_meta(get_the_ID(), 'twitterArtiste', true); ?>" required>
	 	</div>	 	 	
 </div>


<!--<textarea name="gestion-spectacles-editor">
	<?php //echo get_post_meta(get_the_ID(), 'gestion_spectacle_meta', true); ?>
</textarea>-->
